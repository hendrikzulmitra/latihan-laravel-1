@extends('layout.master')

@section('judul')
Halaman Tambah Genre 
@endsection

@section('content')

<form method="POST" action="/genre">
    @csrf
    <div class="form-group">
      <label>Nama Genre</label>
      <input type="text" name="nama"  class="form-control">
    </div>
    @error('nama')
        <div class=" alert-warning">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn-primay">Submit</button>
</form>

@endsection