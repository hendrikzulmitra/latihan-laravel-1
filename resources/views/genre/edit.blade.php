@extends('layout.master')

@section('judul')
Halaman Edit Genre 
@endsection

@section('content')

<form method="POST" action="/genre/{{ $genre->id }}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Genre</label>
      <input type="text" name="nama" value="{{ $genre->nama }}" class="form-control">
    </div>
    @error('nama')
        <div class=" alert-warning">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn-primay">Submit</button>
</form>

@endsection