@extends('layout.master')

@section('judul')
Halaman Tambah Film 
@endsection

@section('content')

<form method="POST" action="/film" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Judul Film</label>
      <input type="text" name="judul"  class="form-control">
    </div>
    @error('judul')
        <div class=" alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Ringkasan Film</label>
      <textarea name="ringkasan" cols="30" rows="10" class="form-control"></textarea>
    </div>
    @error('ringkasan')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Tahun Film</label>
      <input type="number" name="tahun" class="form-control"> 
    </div>
    @error('tahun')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
    <label>Poster Film</label>
      <input type="file" name="poster"  class="form-control">
    </div>
    @error('poster')
        <div class=" alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
    <label>Genre film</label>
      <select name="genre_id"  class="form-control" id="">
        <option value="">---Pilih Genre---</option>
        @foreach ($genre as $item)
          <option value="{{ $item->id }}">{{ $item->nama }}</option>    
        @endforeach
      </select>
    </div>
    @error('genre_id')
        <div class=" alert-warning">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn-primay">Submit</button>
</form>

@endsection