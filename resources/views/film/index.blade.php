@extends('layout.master')

@section('judul')
Halaman List Film 
@endsection

@section('content')

<a href="/film/create" class="btn btn-primary mb-2">Tambah</a>

<div class="row">
    @forelse ($film as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{ asset('gambar/'. $item->poster)}}" class="card-img-top" alt="...">
                <div class="card-body">
                <span class="badge badge-info">{{ $item->genre->nama}}</span>
                <h3>{{ $item->judul }}</h3>
                <p class="card-text">{{Str::limit($item->ringkasan, 30 )}}</p>
                @auth    
                <form action="/film/{{ $item->id }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/film/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/film/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
                @endauth
                </div>
            </div>
        </div> 
    @empty
        <h4>Data Film Belum Ada</h4>
    @endforelse    
</div>

@endsection