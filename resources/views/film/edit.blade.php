@extends('layout.master')

@section('judul')
Halaman Edit Film 
@endsection

@section('content')

<form method="POST" action="/film/{{ $film->id }}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Judul Film</label>
      <input type="text" value="{{ $film->judul }}" name="judul"  class="form-control">
    </div>
    @error('judul')
        <div class=" alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Ringkasan Film</label>
      <textarea name="ringkasan" cols="30" rows="10" class="form-control">{{ $film->ringkasan }}</textarea>
    </div>
    @error('ringkasan')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Tahun Film</label>
      <input type="number" name="tahun" class="form-control"> 
    </div>
    @error('tahun')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
    <label>Poster Film</label>
      <input type="file" name="poster"  class="form-control">
    </div>
    @error('poster')
        <div class=" alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
    <label>Genre film</label>
      <select name="genre_id"  class="form-control" id="">
        <option value="">---Pilih Genre---</option>
        @foreach ($genre as $item)
            @if ($item->id ===$film->genre_id)   
                <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>    
            @else
                <option value="{{ $item->id }}">{{ $item->nama }}</option>
            @endif
        @endforeach
      </select>
    </div>
    @error('genre_id')
        <div class=" alert-warning">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn-primay">Submit</button>
</form>

@endsection