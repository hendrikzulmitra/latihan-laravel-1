@extends('layout.master')

@section('judul')
Halaman Tambah Pemain Baru 
@endsection

@section('content')

<form method="POST" action="/cast">
    @csrf
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" name="nama"  class="form-control">
    </div>
    @error('nama')
        <div class=" alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" name="umur" class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>bio</label>
      <textarea name="bio" cols="30" rows="10" class="form-control"></textarea>
    </div>
    @error('bio')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn-primay">Submit</button>
</form>

@endsection