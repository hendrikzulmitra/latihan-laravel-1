@extends('layout.master')

@section('judul')
Halaman Detail Pemain
@endsection

@section('content')

<h1>Nama Pemain : {{ $cast->nama }}</h1>
<h1>Umur Pemain : {{ $cast->umur }}</h1>
<h1> Bio Pemain : {{ $cast->bio }}</h1>

@endsection