@extends('layout.master')

@section('judul')
Halaman Edit Pemain Baru 
@endsection

@section('content')

<form method="POST" action="/cast/{{ $cast->id }}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" name="nama" value="{{ $cast->nama }}" class="form-control">
    </div>
    @error('nama')
        <div class=" alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" name="umur" value="{{ $cast->umur }}" class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>bio</label>
      <textarea name="bio" cols="30" rows="10" class="form-control">{{ $cast->bio }}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn-primay">Submit</button>
</form>

@endsection