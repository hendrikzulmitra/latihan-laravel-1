<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\profila;

class ProfilaController extends Controller
{
    public function index(){
        $profila = profila::where('user_id', Auth::id())->first();

        return view('profila.index', compact('profila'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required',
        ]);

        $profila = profila::find($id);

        $profila->umur = $request['umur'];
        $profila->bio = $request['bio'];
        $profila->alamat = $request['alamat'];

        $profila->save();

        return redirect('/profila');
    }
}
